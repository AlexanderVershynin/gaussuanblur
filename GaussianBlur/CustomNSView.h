//
//  CustomNSView.h
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/6/21.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomNSView : NSView

@end

NS_ASSUME_NONNULL_END
