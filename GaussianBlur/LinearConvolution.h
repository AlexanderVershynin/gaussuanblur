//
//  LinearConvolution.h
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/11/21.
//

#ifndef LinearConvolution_h
#define LinearConvolution_h

#include "ImageWriter.h"
#include <omp.h>
#include <vector>
#include <thread>
#include <immintrin.h>
#include <iostream>

namespace
{
constexpr bool USE_SIMD_INTRINSICS = true;
}

template< typename T >
inline void Clamp( T& v, T low, T high )
{
	if( v < low )
		v = low;
	else if( v > high )
		v = high;
}

template< typename T >
inline T GetClamped( T v, T low, T high )
{
	Clamp( v, low, high );
	return v;
}

template< typename T>
inline T blur_cast( double v )
{
	return (T)GetClamped<int>( (int)(v > 0.0 ? v + 0.5 : v - 0.5), 0, (std::numeric_limits<T>::max)() );
}

template<>
inline uint8_t blur_cast( double v )
{
	return (uint8_t)GetClamped<int>( (int)(v > 0.0 ? v + 0.5 : v - 0.5), 0, UCHAR_MAX );
}

template<>
inline unsigned short blur_cast( double v )
{
	return (unsigned short)GetClamped<int>( (int)(v > 0.0 ? v + 0.5 : v - 0.5), 0, (std::numeric_limits<unsigned short>::max)() );
}

template<typename T>
struct BufferPair
{
	__attribute__((aligned(32)))
	std::vector<T> input;
	__attribute__((aligned(32)))
	std::vector<T> output;
};

template<typename T, int N>
class LinearConvolution
{
public:
	LinearConvolution(std::vector<float> kernel, unsigned int nRadius, unsigned int nKernelSize,
										float fKernelSum, bool bFadeEdges):
		m_nRadius(nRadius),
		m_nKernelSize(nKernelSize),
		m_fKernelSum(fKernelSum),
		m_kernel(std::move(kernel)),
		m_bFadeEdges(bFadeEdges)
	{
		if constexpr (USE_SIMD_INTRINSICS)
		{
			m_kernel1.resize(m_kernel.size()+4, 0);
			std::copy(m_kernel.begin(), m_kernel.end(), m_kernel1.begin()+1);
			m_kernel2.resize(m_kernel.size()+4, 0);
			std::copy(m_kernel.begin(), m_kernel.end(), m_kernel2.begin()+2);
			m_kernel3.resize(m_kernel.size()+4, 0);
			std::copy(m_kernel.begin(), m_kernel.end(), m_kernel3.begin()+3);
		}
	}

	void horizontalPass(ImageWriter& imageAccessor)
	{
		const size_t nThreads = std::thread::hardware_concurrency();
		__attribute__((aligned(32)))
		std::vector<uint8_t> dataToBlur;
		__attribute__((aligned(32)))
		std::vector<uint8_t> resultData;

		size_t nStripStart = m_nRadius;
		size_t nStripEnd = 0;
		size_t nStripSize = 0;

		#pragma omp parallel for num_threads(static_cast<int>(nThreads)) schedule(static) private(dataToBlur, resultData)
		for (int i = 0; i < imageAccessor.GetHeight(); ++i)
		{
			if (dataToBlur.empty())
			{
				nStripStart = m_nRadius;
				size_t nReminder = (imageAccessor.GetWidth() + 2*m_nRadius) % m_nKernelSize;
				size_t nKernalsCountInStrip = (imageAccessor.GetWidth() + 2*m_nRadius) / m_nKernelSize;
				nStripSize = (nReminder == 0) ? nKernalsCountInStrip * m_nKernelSize :
					(nKernalsCountInStrip + 1) * m_nKernelSize;
				nStripEnd = nStripStart + imageAccessor.GetWidth();
				dataToBlur.resize(nStripSize * static_cast<size_t>(imageAccessor.GetBytesPerPixel()), 0);
				resultData.resize(dataToBlur.size(), 0);
			}
			imageAccessor.CopyRow((uint8_t*)(dataToBlur.data() + nStripStart * N), i);
			processStrip(dataToBlur.data(), resultData.data(), nStripSize, nStripStart, nStripEnd);
			imageAccessor.WriteRow((uint8_t*)(resultData.data() + nStripStart * N), i);
		}
	}

	void verticalPass(ImageWriter& imageAccessor)
	{
		const size_t nThreads = std::thread::hardware_concurrency();
		__attribute__((aligned(32)))
		std::vector<uint8_t> dataToBlur;
		__attribute__((aligned(32)))
		std::vector<uint8_t> resultData;

		size_t nStripStart = m_nRadius;
		size_t nStripEnd = 0;
		size_t nStripSize = 0;

		#pragma omp parallel for num_threads(static_cast<int>(nThreads)) schedule(static) private(dataToBlur, resultData)
		for (int i = 0; i < imageAccessor.GetWidth(); ++i)
		{
			if (dataToBlur.empty())
			{
				nStripStart = m_nRadius;
				size_t nReminder = (imageAccessor.GetHeight() + 2*m_nRadius) % m_nKernelSize;
				size_t nKernalsCountInStrip = (imageAccessor.GetHeight() + 2*m_nRadius) / m_nKernelSize;
				nStripSize = (nReminder == 0) ? nKernalsCountInStrip * m_nKernelSize :
					(nKernalsCountInStrip + 1) * m_nKernelSize;
				nStripEnd = nStripStart + imageAccessor.GetHeight();
				dataToBlur.resize(nStripSize * static_cast<size_t>(imageAccessor.GetBytesPerPixel()), 0);
				resultData.resize(dataToBlur.size(), 0);
			}
			imageAccessor.CopyColumn((uint8_t*)(dataToBlur.data() + nStripStart*N), i);
			processStrip(dataToBlur.data(), resultData.data(), nStripSize, nStripStart, nStripEnd);
			imageAccessor.WriteColumn((uint8_t*)(resultData.data() + nStripStart*N), i);
		}
	}

private:

	void processStrip(T* dataToBlur, T* resultData, size_t nDataSize, size_t nDataStart, size_t nDataEnd)
	{
		if (nDataSize < 0)
			return;
		for (unsigned int j = static_cast<unsigned int>(nDataStart); j < static_cast<unsigned int>(nDataEnd); ++j)
		{


			std::vector<float> accum(N, 0.0f);
			float* pAccum = accum.data();


			if constexpr (USE_SIMD_INTRINSICS)
			{
#if 0
				unsigned int k = 0;
				unsigned int i = 0;
				size_t nKernalStartInStrip =  (-nDataStart + j);
				int nReminder = static_cast<int>(nKernalStartInStrip % 4);
				//std::cout << "Reminder = " << nReminder << std::endl;
				float* kernel = m_kernel.data();
				switch (nReminder)
				{
					case 0:
					default:
						break;
					case 1:
						kernel = m_kernel1.data();
						break;
					case 2:
						kernel = m_kernel2.data();
						break;
					case 3:
						kernel = m_kernel3.data();
						break;
				}
				for (; i < m_nKernelSize; )
				{
					const size_t nKernalStartInStripAligned = nKernalStartInStrip - static_cast<size_t>(nReminder);
					T* pData = dataToBlur + (nKernalStartInStripAligned + i) * N;

					//if ((int)*(pData + 2) != 0)
						//std::cout  << (int)*(pData + 2);
					pAccum[0] += kernel[k+0] * *(pData + 0);
					pAccum[1] += kernel[k+0] * *(pData + 1);
					pAccum[2] += kernel[k+0] * *(pData + 2);
					pAccum[3] += kernel[k+0] * *(pData + 3);

					pAccum[0] += kernel[k+1] * *(pData + 4);
					pAccum[1] += kernel[k+1] * *(pData + 5);
					pAccum[2] += kernel[k+1] * *(pData + 6);
					pAccum[3] += kernel[k+1] * *(pData + 7);

					pAccum[0] += kernel[k+2] * *(pData + 8);
					pAccum[1] += kernel[k+2] * *(pData + 9);
					pAccum[2] += kernel[k+2] * *(pData + 10);
					pAccum[3] += kernel[k+2] * *(pData + 11);

					pAccum[0] += kernel[k+3] * *(pData + 12);
					pAccum[1] += kernel[k+3] * *(pData + 13);
					pAccum[2] += kernel[k+3] * *(pData + 14);
					pAccum[3] += kernel[k+3] * *(pData + 15);
					//assert(pData + 15 - dataToBlur <  nDataSize*N);
					//assert(pAccum[0]/m_fKernelSum < 255.0);
					//assert(pAccum[1]/m_fKernelSum < 255.0);
					//assert(pAccum[2]/m_fKernelSum < 255.0);
					//assert(pAccum[3]/m_fKernelSum < 255.0);

					i += (4 - nReminder);
					k += 4;
					nReminder = 0;
				}
				//std::cout << " i = " << i << " m_nKernelSize = " << m_nKernelSize;
#else


				std::vector<float> tmpAccum1(8, 0.0f);
				std::vector<float> tmpAccum2(8, 0.0f);

				__m128i const maskShuffleCnannels = _mm_set_epi8(0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15);
				__m256i maskKernelFirstTwo = _mm256_set_epi32(0, 0, 0, 0, 1, 1, 1, 1);
				__m256i maskKernelSecondTwo = _mm256_set_epi32(2, 2, 2, 2, 3, 3, 3, 3);
				unsigned int k = 0;
				const size_t nKernalStartInStrip =  (-nDataStart + j);
				const int nReminderStrip = static_cast<int>(nKernalStartInStrip % 4);
				int nReminder = nReminderStrip;
				float* kernel = m_kernel.data();
				switch (nReminderStrip)
				{
					case 0:
					default:
						break;
					case 1:
						kernel = m_kernel1.data();
						break;
					case 2:
						kernel = m_kernel2.data();
						break;
					case 3:
						kernel = m_kernel3.data();
						break;
				}
				for (unsigned int i = 0; i < m_nKernelSize;)
				{
					const size_t nKernalStartInStripAligned = nKernalStartInStrip - static_cast<size_t>(nReminder);
					__m128i vPixels = _mm_load_si128(reinterpret_cast<__m128i*>(dataToBlur + (nKernalStartInStripAligned + i)*N));

					//group channels together by 4 elements
					__m128i vChannels = _mm_shuffle_epi8(vPixels, maskShuffleCnannels);
					//convert 8-bit channels to 32-bit channels
					__m256i v32Channels = _mm256_cvtepu8_epi32(vChannels);
					//convert 32 -bit int channels to 32-bit float channels
					__m256 vfChannels = _mm256_cvtepi32_ps (v32Channels);

					//load kernel chunk to the SIMD 128-bit register
					__m128 vfKernel = _mm_load_ps(kernel + k);

					//convert kernel chunk to 256-bit register
					__m256 vdKernel = _mm256_castps128_ps256(vfKernel);
					//copy 128-bit portion of kernel to high part of 256-bit register
					vdKernel = _mm256_insertf128_ps(vdKernel, vfKernel, 1);

					__m256 vfKernel1 = _mm256_permutevar8x32_ps(vdKernel, maskKernelFirstTwo);
					__m256 res1 = _mm256_mul_ps(vfChannels, vfKernel1);
					//perform SIMD reduction
					res1 = _mm256_hadd_ps (res1, res1);
					res1 = _mm256_hadd_ps (res1, res1);



					__m128i vChannels1 = _mm_srli_si128(vChannels, 8);
					__m256i v32Channels1 = _mm256_cvtepu8_epi32(vChannels1);
					__m256 vfChannels1 = _mm256_cvtepi32_ps (v32Channels1);
					__m256 vfKernel2 = _mm256_permutevar8x32_ps(vdKernel, maskKernelSecondTwo);
					__m256 res2 = _mm256_mul_ps(vfChannels1, vfKernel2);
					//perform SIMD reduction
					res2 = _mm256_hadd_ps (res2, res2);
					res2 = _mm256_hadd_ps (res2, res2);

					_mm256_store_ps (tmpAccum2.data(), res1);
					_mm256_store_ps (tmpAccum1.data(), res2);


					pAccum[0] += tmpAccum1[0];
					pAccum[1] += tmpAccum1[4];
					pAccum[2] += tmpAccum2[0];
					pAccum[3] += tmpAccum2[4];

					i += (4 - nReminder);
					k += 4;
					nReminder = 0;
				}
#endif
			}
			else
			{
				const float* pKernel = m_kernel.data();
				const T* pData = dataToBlur + (j - m_nRadius)*N;
				for (unsigned int i = 0; i < m_nKernelSize*N; ++i)
				{
					unsigned int k = i/N; //kernel index
					unsigned int nChannel = i%N;
					pAccum[nChannel] += pKernel[k] * *pData;
					++pData;
				}
			}

			for (unsigned int nChannel = 0; nChannel < N; ++nChannel)
			{
				if (!m_bFadeEdges)
					pAccum[nChannel] /= m_fKernelSum;
				resultData[j*N + nChannel] = blur_cast<T>(pAccum[nChannel]);
			}
		}
	}

	void processStrip1(T* dataToBlur, T* resultData, size_t nDataSize, size_t nDataStart, size_t nDataEnd)
	{
		if (nDataSize < 0)
			return;
		for (unsigned int j = static_cast<unsigned int>(nDataStart); j < static_cast<unsigned int>(nDataEnd); ++j)
		{


			/*std::vector<float> accum(N, 0.0f);
			float* pAccum = accum.data();
			#pragma omp declare reduction(vec_float_plus : std::vector<float> : std::transform(omp_out.begin(), \
				omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<float>())) \
				initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))*/
			float accum[N] = {};


			size_t nKernalStartInStrip =  (-nDataStart + j);
			int nReminder = static_cast<int>(nKernalStartInStrip % 4);
			float* pKernel = m_kernel.data();
			switch (nReminder)
			{
				case 0:
				default:
					break;
				case 1:
					pKernel = m_kernel1.data();
					break;
				case 2:
					pKernel = m_kernel2.data();
					break;
				case 3:
					pKernel = m_kernel3.data();
					break;
			}

			const size_t nKernalStartInStripAligned = nKernalStartInStrip - static_cast<size_t>(nReminder);
			const T* pData = dataToBlur + nKernalStartInStripAligned * N;

			//#pragma omp simd reduction(+: accum[:N]) aligned(pData:32) aligned(pKernel:32) private(k, nChannel) safelen(4)
			#pragma omp simd reduction(+: accum[:N]) aligned(pData, pKernel:16)
			for (unsigned int i = 0; i < m_nKernelSize*N; ++i)
			{
				unsigned int k = i/N; //kernel index
				unsigned int nChannel = i%N;
				//#pragma omp ordered simd
				accum[nChannel] += pKernel[k] * *pData;
				++pData;
			}




			for (unsigned int nChannel = 0; nChannel < N; ++nChannel)
			{
				if (!m_bFadeEdges)
					accum[nChannel] /= m_fKernelSum;
				resultData[j*N + nChannel] = blur_cast<T>(accum[nChannel]);
			}
		}
	}

	unsigned int m_nRadius;
	unsigned int m_nKernelSize;
	float m_fKernelSum;
	__attribute__((aligned(32)))
	std::vector<float> m_kernel;
	__attribute__((aligned(32)))
	std::vector<float> m_kernel1;
	__attribute__((aligned(32)))
	std::vector<float> m_kernel2;
	__attribute__((aligned(32)))
	std::vector<float> m_kernel3;
	bool m_bFadeEdges;
};


#endif /* LinearConvolution_h */
