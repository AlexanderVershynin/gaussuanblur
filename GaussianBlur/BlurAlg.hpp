//
//  BlurAlg.hpp
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/17/21.
//

#ifndef BlurAlg_hpp
#define BlurAlg_hpp

#include <stdio.h>
#include <CoreGraphics/CoreGraphics.h>

void RIPPerfectGaussianBlur(CGContextRef toBlur, float fSigma, unsigned int nRadius, bool bFadeEdges);

#endif /* BlurAlg_hpp */
