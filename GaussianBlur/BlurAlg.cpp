//
//  BlurAlg.cpp
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/17/21.
//

#include "BlurAlg.hpp"
#include <CoreGraphics/CoreGraphics.h>
#include "LinearConvolution.h"

template< typename T, int N >
void RIPPerfectGaussianBlurT(CGContextRef toBlur, float fSigma, unsigned int nRadius, bool bFadeEdges)
{
	unsigned int nKernelSize = 2*nRadius + 1;
	unsigned int nReminder = nKernelSize%16;
	const unsigned int nKernelSize16Aligned = (nReminder == 0)? nKernelSize : nKernelSize + 16 - nReminder;
	__attribute__((aligned(32)))
	std::vector<float> kernel;
	float fKernelSum = 0.0f;
	kernel.resize(nKernelSize16Aligned, 0.0f);
	{
		//fill kernel weights
		float fSum = 0.0f;
		for (auto it = kernel.begin(); it != kernel.end() - (16 - nReminder); ++it)
		{
			unsigned int idx = static_cast<unsigned int>(std::distance(kernel.begin(), it));
			const float cf2SigmaSquared = 2.0f * fSigma * fSigma;
			const float cfSigmaSqrt2Pi = fSigma * 2.506628274631f; // 2.506628274631 == sqrt( 2 * pi );
			const float x = static_cast<float>(idx) - static_cast<float>(nRadius);
			const float fx = static_cast<float>(exp(static_cast<double>(-x * x / cf2SigmaSquared ))) / cfSigmaSqrt2Pi;
			*it = fx;
			fSum += *it;
		}
		for (float& kv: kernel)
			kv /= fSum;
		fKernelSum = fSum;
	}

	LinearConvolution<T, N> convoluter{std::move(kernel), nRadius, nKernelSize16Aligned, fKernelSum, bFadeEdges};
	ImageWriter imageAccessor(toBlur);
	convoluter.horizontalPass(imageAccessor);
	convoluter.verticalPass(imageAccessor);
}

void RIPPerfectGaussianBlur(CGContextRef toBlur, float fSigma, unsigned int nRadius, bool bFadeEdges)
{
	RIPPerfectGaussianBlurT<uint8_t, 4>(toBlur, fSigma, nRadius, bFadeEdges);
}
