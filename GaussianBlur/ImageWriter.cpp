//
//  ImageWriter.cpp
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/11/21.
//

#include "ImageWriter.h"
#include <algorithm>
#include <CoreGraphics/CoreGraphics.h>

ImageWriter::ImageWriter(CGContextRef bmp):
	m_bmp(bmp)
{
}

int ImageWriter::GetWidth() const
{
	return static_cast<int>(CGBitmapContextGetWidth(m_bmp));
}

int ImageWriter::GetHeight() const
{
	return static_cast<int>(CGBitmapContextGetHeight(m_bmp));
}

int ImageWriter::GetBytesPerPixel() const
{
	return static_cast<int>(CGBitmapContextGetBitsPerPixel(m_bmp)/8);
}

void ImageWriter::WriteRow(const uint8_t* pSrc, size_t row)
{
	uint8_t* pData = static_cast<uint8_t*>(CGBitmapContextGetData(m_bmp));
	size_t nRowSize = CGBitmapContextGetBytesPerRow(m_bmp);
	size_t nDataRowSize = GetWidth() * GetBytesPerPixel();
	pData += nRowSize * row;
	std::copy(pSrc, pSrc + nDataRowSize, pData);
}

void ImageWriter::WriteColumn(const uint8_t* pSrc, size_t column)
{
	uint8_t* pData = static_cast<uint8_t*>(CGBitmapContextGetData(m_bmp));
	size_t nRowSize = GetWidth() * GetBytesPerPixel();//CGBitmapContextGetBytesPerRow(m_bmp);
	size_t nBytesPerPixel = GetBytesPerPixel();
	for (size_t i = 0; i < GetHeight(); ++i)
	{
		uint8_t* pCurData = pData + nRowSize * i + column * nBytesPerPixel;
		std::copy(pSrc, pSrc + nBytesPerPixel, pCurData);
		pSrc += nBytesPerPixel;
	}
}

void ImageWriter::CopyRow(uint8_t* pDst, size_t row) const
{
	uint8_t* pData = static_cast<uint8_t*>(CGBitmapContextGetData(m_bmp));
	size_t nRowSize = CGBitmapContextGetBytesPerRow(m_bmp);
	size_t nDataRowSize = GetWidth() * GetBytesPerPixel();
	pData += nRowSize * row;
	std::copy(pData, pData + nDataRowSize, pDst);
}

void ImageWriter::CopyColumn(uint8_t* pDst, size_t column) const
{
	uint8_t* pData = static_cast<uint8_t*>(CGBitmapContextGetData(m_bmp));
	size_t nRowSize = GetWidth() * GetBytesPerPixel();//CGBitmapContextGetBytesPerRow(m_bmp);
	size_t nBytesPerPixel = GetBytesPerPixel();
	for (size_t i = 0; i < GetHeight(); ++i)
	{
		uint8_t* pCurData = pData + nRowSize * i + column * nBytesPerPixel;
		std::copy(pCurData, pCurData + nBytesPerPixel, pDst);
		pDst += nBytesPerPixel;
	}
}
