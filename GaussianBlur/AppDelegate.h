//
//  AppDelegate.h
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/6/21.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

