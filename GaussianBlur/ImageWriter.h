//
//  ImageWriter.h
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/11/21.
//

#ifndef ImageWriter_h
#define ImageWriter_h

#include <cstdint>
#include <CoreGraphics/CGContext.h>

class IImageWriter
{
public:
	virtual void WriteRow(const uint8_t* pSrc, size_t row) = 0;
	virtual void WriteColumn(const uint8_t* pSrc, size_t column) = 0;

	virtual void CopyRow(uint8_t* pDst, size_t row) const = 0;
	virtual void CopyColumn(uint8_t* pDst, size_t column) const = 0;

	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;
	virtual int GetBytesPerPixel() const = 0;
};

class ImageWriter: public IImageWriter
{
public:
	explicit ImageWriter(CGContextRef bmp);

	void WriteRow(const uint8_t* pSrc, size_t row) override;
	void WriteColumn(const uint8_t* pSrc, size_t column) override;

	void CopyRow(uint8_t* pDst, size_t row) const override;
	void CopyColumn(uint8_t* pDst, size_t column) const override;

	int GetWidth() const override;
	int GetHeight() const override;
	int GetBytesPerPixel() const override;


private:
	mutable CGContextRef m_bmp;
};

#endif /* ImageWriter_h */
