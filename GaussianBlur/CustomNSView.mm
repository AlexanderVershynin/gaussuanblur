//
//  CustomNSView.m
//  GaussianBlur
//
//  Created by Alexander Vershynin on 4/6/21.
//

#import "CustomNSView.h"
#include "BlurAlg.hpp"
#include <iostream>


@implementation CustomNSView

- (void)drawRect:(NSRect)dirtyRect {
	CGColorSpaceRef colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
	CGRect boundingBox = self.bounds;

	CGContextRef context = CGBitmapContextCreate(nullptr,
																							 boundingBox.size.width,
																							 boundingBox.size.height,
																							 8,
																							 4 * boundingBox.size.width,
																							 colorSpace,
																							 kCGImageAlphaPremultipliedLast);
	CGRect clientBox{0, 0, boundingBox.size.width, boundingBox.size.height};
	CGContextClearRect(context, clientBox);


	CGContextRef viewContext = [[NSGraphicsContext currentContext] CGContext];


	float fShift = std::min(boundingBox.size.height, boundingBox.size.width)*0.05;
	CGContextSetRGBStrokeColor(context, 0, 0, 1, .5);
	CGContextMoveToPoint(context, fShift, fShift);
	CGContextAddLineToPoint(context, boundingBox.size.width - fShift, boundingBox.size.height/2);
	CGContextAddLineToPoint(context, fShift, boundingBox.size.height - fShift);
	CGContextClosePath(context);
	CGContextSetRGBFillColor(context, 0, 0, 1, .5);
	CGContextSetLineJoin(context, kCGLineJoinRound);
	CGContextDrawPath(context, kCGPathFillStroke);


	auto start = std::chrono::system_clock::now();
	RIPPerfectGaussianBlur(context, 5.0f, 50, true);
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	std::cout << "finished computation at " << std::ctime(&end_time)
								<< "elapsed time: " << elapsed_seconds.count() << "s\n";


	CGImageRef img = CGBitmapContextCreateImage(context);
	//CGContextClearRect(viewContext, clientBox);
	CGContextDrawImage(viewContext, boundingBox, img);

	CGImageRelease(img);
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
}

@end
